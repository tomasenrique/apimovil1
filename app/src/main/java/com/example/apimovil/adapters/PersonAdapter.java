package com.example.apimovil.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.apimovil.R;
import com.example.apimovil.entities.Person;

import java.util.ArrayList;

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.PersonHolder> {

    private ArrayList<Person> personList;

    // Builder
    public PersonAdapter(ArrayList<Person> personList) {
        this.personList = personList;
    }

    @NonNull
    @Override
    public PersonHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.person_list_adapter, parent, false);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(layoutParams);
        return new PersonHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PersonHolder holder, int position) {
//        holder.image.setImageResource(R.mipmap.ic_launcher_round);
        holder.image.setImageResource(R.drawable.imagen_prueba);

        holder.name.setText(personList.get(position).getName());
        holder.dni.setText(personList.get(position).getDni());
        holder.age.setText(String.valueOf(personList.get(position).getAge()));
        holder.birthday.setText(personList.get(position).getBirthday()); // OJO
        holder.married.setText(String.valueOf(personList.get(position).isMarried()));
        holder.height.setText(String.valueOf(personList.get(position).getHeight()));
    }

    @Override
    public int getItemCount() {
        return personList.size();
    }

    public class PersonHolder extends RecyclerView.ViewHolder {
        TextView name, dni, age, birthday, married, height;
        ImageView image;

        public PersonHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.imageViewPerson);
            name = itemView.findViewById(R.id.textViewName);
            dni = itemView.findViewById(R.id.textViewDni);
            age = itemView.findViewById(R.id.textViewAge);
            birthday = itemView.findViewById(R.id.textViewBirthday);
            married = itemView.findViewById(R.id.textViewMarried);
            height = itemView.findViewById(R.id.textViewHeight);
        }
    }
}
