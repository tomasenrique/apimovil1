package com.example.apimovil.repository;

import com.example.apimovil.entities.Person;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface PersonRepositoryApi {

    @GET("show")
    Call<ArrayList<Person>>getPersonListApi(); // Get a list of person from database


    @POST("add")
    Call<Person> addPersonApi(@Body Person person); // Add a person to database

}
