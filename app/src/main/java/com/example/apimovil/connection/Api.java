package com.example.apimovil.connection;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {

    private Retrofit retrofit;

    // Builder
    protected Api() {
    }

    /**
     * This method create the connection and give back the api connection object.
     *
     * @param urlLink this is url to api
     * @return api connection
     */
    protected Retrofit getConnectionApi(String urlLink) {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(urlLink)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }

}
