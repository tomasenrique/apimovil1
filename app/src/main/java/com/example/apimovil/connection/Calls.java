package com.example.apimovil.connection;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.apimovil.InterfaceCall.CallsMethodsPerson;
import com.example.apimovil.adapters.PersonAdapter;
import com.example.apimovil.entities.Person;
import com.example.apimovil.repository.PersonRepositoryApi;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Calls implements CallsMethodsPerson {

    private Retrofit retrofit;
    private Api api;
    private Links links;
    private Context context;
    private RecyclerView recyclerView;
    private String PERSON;
    private PersonRepositoryApi personRepositoryApi;

    // Builders
    public Calls(Context context) {
        api = new Api();
        links = new Links();
        this.context = context;
    }

    public Calls(RecyclerView recyclerView, Context context) {
        api = new Api();
        links = new Links();
        this.recyclerView = recyclerView;
        this.context = context;
    }


    @Override
    public void getPersonList() {
        PERSON = "PERSONS LIST";
        retrofit = api.getConnectionApi(links.getLink(links.PERSON));
        personRepositoryApi = retrofit.create(PersonRepositoryApi.class);
        Call<ArrayList<Person>> arrayListCallAnswer = personRepositoryApi.getPersonListApi();

        arrayListCallAnswer.enqueue(new Callback<ArrayList<Person>>() {
            @Override
            public void onResponse(Call<ArrayList<Person>> call, Response<ArrayList<Person>> response) {
                if (response.isSuccessful()) {
                    ArrayList<Person> personListAnwer = response.body();

                    PersonAdapter personAdapter = new PersonAdapter(personListAnwer);
                    recyclerView.setAdapter(personAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(context));

                    // For showing the arriving data from api
                    for (int i = 0; i < personListAnwer.size(); i++) {
                        Log.i(PERSON, personListAnwer.get(i).getName());
                    }
                } else {
                    Log.i(PERSON, "Is no possible download data.");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Person>> call, Throwable t) {
                Log.i(PERSON, "PersonList onFailure connection: " + t.getMessage());
            }
        });
    }

    @Override
    public void addPerson(Person person) {
        PERSON = "ADD PERSON";
        retrofit = api.getConnectionApi(links.getLink(links.PERSON));
        personRepositoryApi = retrofit.create(PersonRepositoryApi.class);
        Call<Person> addPersonAnswer = personRepositoryApi.addPersonApi(person);

        addPersonAnswer.enqueue(new Callback<Person>() {
            @Override
            public void onResponse(Call<Person> call, Response<Person> response) {
                Person personAnswer = response.body();
                if (personAnswer != null) {
                    Toast toast = Toast.makeText(context, "Data added to list", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 400);
                    toast.show();
                } else {
                    Toast toast = Toast.makeText(context, "Do can't add person to list " + response.errorBody(), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 400);
                    toast.show();
                }
            }

            @Override
            public void onFailure(Call<Person> call, Throwable t) {
                Log.i(PERSON, "Connection to api failed: " + t.getMessage());
            }
        });
    }
}
