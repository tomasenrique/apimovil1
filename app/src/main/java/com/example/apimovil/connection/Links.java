package com.example.apimovil.connection;

import java.util.HashMap;

public class Links {
    private final String LOCALHOST = "http://192.168.1.115:";
    private final String PORT = "8081";
    private final String ROUTE = "/api2/";

    private final String LINK_PERSON = LOCALHOST + PORT + ROUTE + "person/";

    protected final String PERSON = "person";

    private HashMap<String, String> ulrLinkApi;

    // Builder
    protected Links() {
        ulrLinkApi = new HashMap<>();
        ulrLinkApi.put(PERSON, LINK_PERSON);
    }


    // Method

    /**
     * This method it serves for get link methods to the api
     *
     * @param keyLink it will be the key of url link
     * @return url link to the api
     */
    protected String getLink(String keyLink) {
        return ulrLinkApi.get(keyLink);
    }


}
