package com.example.apimovil.entities;

import java.io.Serializable;

public class Person implements Serializable {

    private Long id;
    private String name;
    private String dni;
    private int age;
    private String birthday;
    private boolean married; // true or false
    private double height; // in 1.64

    // Builder
    public Person() {
    }

    public Person(String name, String dni, int age, String birthday, boolean married, double height) {
        this.name = name;
        this.dni = dni;
        this.age = age;
        this.birthday = birthday;
        this.married = married;
        this.height = height;
    }

    // Setter and Getter
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public boolean isMarried() {
        return married;
    }

    public void setMarried(boolean married) {
        this.married = married;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}
