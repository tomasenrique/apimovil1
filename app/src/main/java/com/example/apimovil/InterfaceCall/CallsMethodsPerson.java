package com.example.apimovil.InterfaceCall;

import com.example.apimovil.entities.Person;

public interface CallsMethodsPerson {

    void getPersonList(); // Get a person list

    void addPerson(Person person); // Add a person to list of database


}
