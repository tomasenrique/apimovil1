package com.example.apimovil.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.apimovil.R;
import com.example.apimovil.connection.Calls;

public class ActivityShowPersonList extends AppCompatActivity {

    private RecyclerView recyclerViewPersonList;
    private Calls callsPersonList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_person_list);

        recyclerViewPersonList = findViewById(R.id.recyclerViewPersonList); // To show person list
        callsPersonList = new Calls(recyclerViewPersonList, getApplicationContext());
    }


    @Override
    protected void onResume() {
        super.onResume();

        callsPersonList.getPersonList();
    }
}