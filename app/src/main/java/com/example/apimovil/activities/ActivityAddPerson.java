package com.example.apimovil.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.apimovil.R;
import com.example.apimovil.connection.Calls;
import com.example.apimovil.entities.Person;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;


public class ActivityAddPerson extends AppCompatActivity {

    private TextView name, dni, age, birthday, height;
    private RadioButton marriedSi, marriedNo;
    private Button addPhoto, addPerson, clean;
    private ImageView imageViewPerson;
    private Calls callsAddPerson;


    private static final int GALLERY_CODE = 0;
    private static final int CAMERA_CODE = 1;
    private String imageFilePath;
    private Uri filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_person);

        name = findViewById(R.id.editTextName);
        dni = findViewById(R.id.editTextDni);
        age = findViewById(R.id.editTextAge);
        birthday = findViewById(R.id.textViewDateBirthday);
        marriedSi = findViewById(R.id.radioButtonSi);
        marriedNo = findViewById(R.id.radioButtonNo);
        height = findViewById(R.id.editTextHeight);

        addPhoto = findViewById(R.id.buttonAddPhoto);
        addPerson = findViewById(R.id.buttonAddPerson);
        clean = findViewById(R.id.buttonClean);

        imageViewPerson = findViewById(R.id.imageViewPerson);

        callsAddPerson = new Calls(this);


    }


    @Override
    protected void onResume() {
        super.onResume();

        birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker(); // Show picker to add date of birthday
            }
        });

        addPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Person person = getPersonData();
                callsAddPerson.addPerson(person);
            }
        });

        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseCameraOrGallery();
            }
        });

        clean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cleanFields();
            }
        });

    }

    /**
     * This method clean the fields of the view
     */
    private void cleanFields() {
        name.setText("");
        dni.setText("");
        age.setText("");
        birthday.setText("Date birthday");
        marriedSi.setChecked(false);
        marriedNo.setChecked(false);
        height.setText("");
    }


    /**
     * This method get person data
     *
     * @return a person object with data.
     */
    private Person getPersonData() {
        String textName, textDni, textBirthday;
        int textAge;
        boolean textMarried;
        double textHeight;

        textName = name.getText().toString();
        textDni = dni.getText().toString();
        textAge = Integer.parseInt(age.getText().toString());
        textBirthday = String.valueOf(birthday.getText());
        textMarried = marriedSi.isChecked() ? true : false; // function ternary
        textHeight = Double.parseDouble(height.getText().toString());
        return new Person(textName, textDni, textAge, textBirthday, textMarried, textHeight);
    }


    /**
     * This method add date of birthday
     */
    private void showDatePicker() {
        // To assign current date of system
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String aux; // to save date concatenated
                month += 1; // To correct month and day of 0 to 11 => 1 to 12

                if (month < 10 && dayOfMonth < 10) {
                    aux = year + "-" + "0" + month + "-" + "0" + dayOfMonth;
                    birthday.setText(aux);
                } else if (month < 10) {
                    aux = year + "-" + "0" + month + "-" + dayOfMonth;
                    birthday.setText(aux);
                } else if (dayOfMonth < 10) {
                    aux = year + "-" + month + "-" + "0" + dayOfMonth;
                    birthday.setText(aux);
                } else {
                    aux = year + "-" + month + "-" + dayOfMonth;
                    birthday.setText(aux);
                }
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    // =============================================================================================
    // =============================================================================================


    /**
     * Choose between take photo or access storage to images
     */
    private void chooseCameraOrGallery() {
        final String[] chooseDialogItems = getResources().getStringArray(R.array.chooseImageDialog);
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityAddPerson.this);
        builder.setTitle(R.string.chooseImageDialogTittle);
        builder.setItems(chooseDialogItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (chooseDialogItems[which].equals(getResources().getString(R.string.chooseImageDialogCamera))) {
                    openCamera();
                } else if (chooseDialogItems[which].equals(getResources().getString(R.string.chooseImageDialogImage))) {
                    openGallery();
                } else if (chooseDialogItems[which].equals(getResources().getString(R.string.chooseImageDialogCancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * Create a image with the camera
     *
     * @return
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();
        return image;
    }

    /**
     * Access to camera to take photos
     */
    private void openCamera() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (pictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null; // For to save imagen
            try {
                photoFile = createImageFile(); // save created imagen in the file
            } catch (IOException ex) {
                Log.e("ERROR", Objects.requireNonNull(ex.getMessage()));

            }
            if (photoFile != null) { // Verify that file isn't null
                Uri photoURI = FileProvider.getUriForFile(this, "com.example.apimovil.provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(pictureIntent, CAMERA_CODE);
            }
        }
    }

    /**
     * Access to storage movil to choose an imagen
     */
    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, GALLERY_CODE);
    }

    /**
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CAMERA_CODE:
                    filePath = Uri.fromFile(new File(imageFilePath));
                    imageViewPerson.setImageURI(filePath);
                    break;
                case GALLERY_CODE:
                    if (data != null) {
                        filePath = data.getData();
                        imageViewPerson.setImageURI(filePath);
                        imageViewPerson.setTag(String.valueOf(filePath));
                    }
                    break;
            }
        }
    }


}