package com.example.apimovil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.apimovil.activities.ActivityAddPerson;
import com.example.apimovil.activities.ActivityShowPersonList;

public class MainActivity extends AppCompatActivity {

    private Button buttonShowPersonList, buttonAddPersonList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonShowPersonList = findViewById(R.id.buttonPersonList);
        buttonAddPersonList =  findViewById(R.id.buttonAddPersonList);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        buttonShowPersonList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityShowPersonList.class);
                startActivity(intent);
            }
        });


        buttonAddPersonList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityAddPerson.class);
                startActivity(intent);
            }
        });
    }
}